//Ashwin Ashokan  UIN: 230002171


#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>
#include <fstream>
#include <unistd.h>
#include <string>
#include <cmath>
#include <list>
#include <cstring>
#include <cstdlib>
using namespace std;

struct block //strucutre of a block
{
	unsigned long long int refAddress;
	bool validBit = false;//to check if the block is valid(i.e. Initialized)
};
//structure of a set.
struct set
{	
	std::list<block> blocks;//has a list of blocks(double Linked Lists).
	};

struct cache
{
	std::vector<set> setz;
	int totalSets;
	int tagBits;
	int indexBits;
	unsigned int blocksPerSet;
	int totalBlocks;
	int byteOffsetBits;
	int nk;//capacity of caches in Kilobytes
	int assoc; //associavity of cache
	int blockSize;//block size in Bytes
	char repl; //type of replacement policy 'l' for LRU and 'r' for random	
	};

//functions used by the program
void cmdLineChar_int(cache&,int,char**);//loads commandline parameters into the created cache Structure
void constructCache(cache&,int totalSets,int blockPerSet);//creates the cache Structure of defined by cmdline param
void pipeOpen(ifstream&);//Creates a filestream to load from the trace file
vector<string> strDelimiter(const string&);//splits string obtained from the trace file to r/w and address
string readPipe(ifstream&);//returns a line from the traceFile
unsigned long long int setValue(unsigned long long int address,int tagBits,int byteOffsetBits);//finds the corresponding set
bool compareTag(const unsigned long long int,const unsigned long long int, const int);//compares the tag and address in a block 
void miss(cache&,unsigned long long int,int,string);//updates the block based on replacement policy and writes the tagvalue to block
void showList ( list <block> myList);//debugging pupose to read the contents of the set.

int main(int argc, char** argv)//main function
{
ifstream inFile;//local var to hold the inputText file
cache cacheSim;//cacheStructure
cmdLineChar_int(cacheSim,argc,argv);//loads cmdLine arg to cacheStructure
constructCache(cacheSim,cacheSim.totalSets,cacheSim.blocksPerSet);//constructs the Cache cacheSim with appropriate number of set and blocks allocated dynamically when needed

pipeOpen(inFile);
vector<string> splitStrings;//vector of strings to hold the r/w variable and the 64bit address
long int readMissCount=0;
long int writeMissCount=0;
double readAccess=0;
double writeAccess = 0;
double totalAccess = 0;
unsigned long long int address=0;
int setNumber=0;
bool hitCheck = false;//to check if there is a hit and stop searching the rest of the blocks
while(inFile.good())//checks if the file hasnt reached the end.
		{
		hitCheck = false;
		splitStrings = strDelimiter(readPipe(inFile));//splits the read string to r/w and hex address
		if(!inFile.good()) break;//checks if the file has reached EOF
		char hexVal[splitStrings[1].size()+1];//char array to hold the hex address.
		strcpy(hexVal,splitStrings[1].c_str());//converts the string to char array for conversion to ull int d_type 
		address = (int)strtol(hexVal,NULL,16);//char array of 64bit address converted to d_type ull int.
		if(splitStrings[0]=="r") readAccess++;//Read Access count
		if(splitStrings[0]=="w") writeAccess++;//Write Access Count
		totalAccess++;//Counts the total memAccess
		setNumber = setValue(address,cacheSim.tagBits,cacheSim.byteOffsetBits);//calculates the set number based on the cache Size parameters
		list<block>::iterator it;//iterator to loop through and check the blocks for the identified set
		
		for(it=cacheSim.setz[setNumber].blocks.begin();it!=cacheSim.setz[setNumber].blocks.end();++it)//loops through all the block of the identified set
		{	
	
			if(compareTag(it->refAddress,address,cacheSim.tagBits)==true)//checks for a hit
			{
				if(it->validBit==true)//and check if the valid bit is true and the block is initialized. Since initially the block have 0 in ref address, 0 hex value gets assigned and this step prevents that error.
				{
					hitCheck = true;
					block tempBlock = *it;//temp value to hold the hit block
					if(cacheSim.repl == 'l')
					{
					cacheSim.setz[setNumber].blocks.erase(it);//delete the hit block and shift pointer of the dl list
						}
					else
					{
					 cacheSim.setz[setNumber].blocks.pop_back();
					}
					cacheSim.setz[setNumber].blocks.push_front(tempBlock);//LRU replacement policy to pushes the list to the front of the list
					break;
				}
				else//debugging condition to check if the index acces unallocated memory of the set and returns an error message
				{	
					hitCheck = true;
					cerr<<"When working on Uninitialized blocks"<<endl;
					exit(1);
					break;
					}
				}
			}
		if(hitCheck == false)//if there is no hit in the current set it goes to this condition 
		{	if(splitStrings[0]=="r") readMissCount++;
			else if(splitStrings[0]=="w") writeMissCount++;
			miss(cacheSim,address,setNumber,splitStrings[1]);
		}
		
		}
//declaration and cout to print the obtained results as per the assignments standard
double percentMiss = ((readMissCount+writeMissCount)/totalAccess)*100;
double readMissPercent =((readMissCount/readAccess)*100);
double writeMissPercent =(((writeMissCount)/(writeAccess))*100);
cout<<fixed<<setprecision(6)<<readMissCount+writeMissCount<<" "<<percentMiss<<"%"<<" "<<readMissCount<<" "<<readMissPercent<<"%"<<" "<<writeMissCount<<" "<<writeMissPercent<<"%"<<endl; 
return 0;
}


void showList ( list <block> myList)
{
	/*
	 * arg1: copies a list of block by value
	 * it iterates throught the list and displays the address present in each Block */
	list<block>::iterator it;
	for (it=myList.begin();it!=myList.end();++it)
	{
		cout<<it->refAddress<<" ";
	}
cout<<endl;
}





void cmdLineChar_int(cache& cacheStruct,int argc, char** argv)
{
	/*
	 * arg1: call by reference the data type struct(cache)
	 * arg2: (int) call by value it copies the cmd line param 1
	 * arg3: (char**) call by reference it copies the cmd line param 2
	 * Def: it assigns the command line parameters (cache description) to the variables of the cache structure by ref */
	
	if(argc != 5) cerr<<"There must be only 5 cmd Line Arguments including the cache.c Call"<<endl;//checks that all the arguments are displayed
	
	else
	{
		cacheStruct.nk = stoi(argv[1])*(1<<10);//since kilobytes we multiply by 1000.
		cacheStruct.assoc = stoi(argv[2]);
		cacheStruct.blockSize = stoi(argv[3]);
		cacheStruct.repl = *argv[4];
		cacheStruct.totalBlocks = cacheStruct.nk/cacheStruct.blockSize;// total cache size / blocksize
		cacheStruct.totalSets = (cacheStruct.nk/cacheStruct.blockSize)/cacheStruct.assoc;// Total Blocks/associavity
		cacheStruct.indexBits = log2(cacheStruct.totalSets);//bits allocated based on the total number of sets
		cacheStruct.blocksPerSet = cacheStruct.totalBlocks/cacheStruct.totalSets;
		cacheStruct.tagBits = 64 - cacheStruct.indexBits - log2(cacheStruct.blockSize);
		cacheStruct.byteOffsetBits = log2(cacheStruct.blockSize);
	}
}

void constructCache(cache& cacheDesign,int totalSets,int blockPerSet)//construct the cache based on provided cmdlinarg
{
	/*
	 * arg1: cache struct call by ref
	 * arg2: total number of sets in cache Structure (int)
	 * arg3: total blocks per set of the cache (int)
	 * def: it creates a cache by assigning the appropriate number of sets and block as per desciption and returns void since it updates through call by reference */
	
	set tempSet;//initialize a dummy set that can be pushed inside the cache vector
	cacheDesign.setz.reserve(totalSets);
	
	for(int i = 0;i<totalSets;++i)
	{
		cacheDesign.setz.push_back(tempSet);
		tempSet.blocks.clear();
	}
}

void pipeOpen(ifstream& inFile)
{
	/*
	 * arg1: call by ref of d_type ifstream that opens the stdIn for getting the traces and values*/
	
	//inFile.open("test.txt");//debugging purpose
	inFile.open("/dev/stdin");
	if(!inFile) cerr<<"No file to pipe into the C++ Program"<<std::endl;
}

vector<string> strDelimiter(const string& trace)
{
	/*
	 * arg1 = const string call by ref
	 * def: if splits the incoming string into r/w and hex address
	 */
	int pos = trace.find(" ");
	vector<string> splitTrace;
	splitTrace.push_back(trace.substr(0,pos));
	splitTrace.push_back(trace.substr(pos+1));
	return splitTrace;	
}

string readPipe(ifstream& inFile)
{
	/*arg1: call by ref of d_type ifstream
	 * def: read the line of the passed ifstream file.
	 * ret: returns a string of the line read
	 */
	
	string readTrace;
	if(inFile.good())
	{
		getline(inFile,readTrace);
		return readTrace;
	}
	else return "EOF";
}

unsigned long long int setValue(unsigned long long int address,int tagBits,int byteOffsetBits)
{
	/*
	 * arg1: ull int call by val
	 * arg2: no of tag bits (int)
	 * arg3: no of ByteoffsetBits (int)
	 * def: it returns the ull_int d_type of the setNumber given by the address, tagbit and byteoffsetbits in 64bit address line
	*/	
	int indexEnd = 64 - tagBits - 1;
	int indexStart = byteOffsetBits;
	//calculating the index Value:
	int base = 0;
	unsigned long long int dec_val = 0;
	for(int i =indexStart;i<=indexEnd;++i)
	{
		if(((address>>i)&1) == 1) 
		{ 	
			dec_val+=pow(2,base);
		}
		++base;
	}
	return dec_val;
}




bool compareTag(const unsigned long long int traceAddress, const unsigned long long int blockAddress,const int tagSize_bits)
{	
	/*
	 * arg1: ull_int address from the trace file
	 * arg2: ull_int identification address(tag+byteOffset+setNumber)  
	 * arg3: int number of tagBits for comparison
	 * def: it compares the address in block and the incoming trace address to find if it is a hit or not
	 * ret: a bool if hit(1) else 0
	 */  
	for(int i = 63;i>=64-tagSize_bits;i--)
	{
		int trc = traceAddress>>i;
		int blk = blockAddress>>i;
		if((trc&1) != (blk&1)) return false;
	} 
	return true;
	}

void miss(cache& cacheSim,unsigned long long int address,int setNumber,string splitStrings)
{
	/*
	 * arg1: call by ref dtype: cache
	 * arg2: ull int:trace address that must be loaded on to the cache based on the replacement Policy
	 * arg3: int: setIndex to identify the set
	 * arg4: string: debugging purpose.
	 * def: it updates the cache based on replacement policy and puts the trace address into the block
	 */ 
	
	
	if(cacheSim.setz[setNumber].blocks.size() < cacheSim.blocksPerSet)
	{
		block tempBlock;
		tempBlock.refAddress =address;
		tempBlock.validBit = true;
		cacheSim.setz[setNumber].blocks.push_front(tempBlock);
	}
	
	else
	{
		block tempBlock;
		if(cacheSim.repl=='l')
		{
			cacheSim.setz[setNumber].blocks.pop_back();//LRU Replacement
		}
		else 											//Random Replacement using rand() function
		{
		 list<block>::iterator it = cacheSim.setz[setNumber].blocks.begin();
		 advance(it,rand()%(cacheSim.blocksPerSet-1));
		 cacheSim.setz[setNumber].blocks.erase(it);
			}
		tempBlock.refAddress = address;
		tempBlock.validBit = true;
		cacheSim.setz[setNumber].blocks.push_front(tempBlock);
	}
}
